// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'enums/playback_rate.dart';
import 'enums/player_state.dart';
import 'enums/youtube_error.dart';
import 'meta_data.dart';

/// Youtube Player value
class YoutubePlayerValue {
  /// The duration, current position, buffering state, error state and settings
  /// of a [YoutubePlayerController].
  YoutubePlayerValue({
    this.isReady = false,
    this.hasPlayed = false,
    this.position = const Duration(),
    this.buffered = 0.0,
    this.isFullScreen = false,
    this.volume = 100,
    this.playerState = PlayerState.unknown,
    this.playbackRate = DEPRECATEDPlaybackRate.normal,
    this.playbackQuality,
    this.error = YoutubeError.none,
    this.metaData = const YoutubeMetaData(),
    this.isUpdatingTime = false,
    this.isUpdatingBuffered = false,
    this.isSeeking = false,
    this.hasMetadata = false,
    this.isMuted = false,
    this.isReplaying = false,
  });

  /// Returns true when the player is ready to play videos.
  final bool isReady;

  /// Returns true once the video start playing for the first time.
  final bool hasPlayed;

  /// The current position of the video.
  final Duration position;

  /// The percent up to which the video is buffered.
  final double buffered;

  /// Reports true if video is fullscreen.
  final bool isFullScreen;

  /// The current volume assigned for the player.
  final int volume;

  /// The current state of the player defined as [PlayerState].
  final PlayerState playerState;

  /// The current video playback rate defined as [DEPRECATEDPlaybackRate].
  final double playbackRate;

  /// Reports the error code as described [here](https://developers.google.com/youtube/iframe_api_reference#Events).
  ///
  /// See the onError Section.
  final YoutubeError error;

  /// Returns true is player has errors.
  bool get hasError => error != YoutubeError.none;

  /// Reports the current playback quality.
  final String? playbackQuality;

  /// Returns meta data of the currently loaded/cued video.
  final YoutubeMetaData metaData;

  /// Returns whether or not the video has been muted.
  final bool isMuted;

  /// Returns whether or not the video has just ended and is starting again.
  bool isReplaying;

  Duration get videoLength => metaData.duration;

  String get videoTitle => metaData.title;

  String get videoAuthor => metaData.author;

  String get videoId => metaData.videoId;

  /// Returns whether the provided [newValue] has changed its [playerState] to
  /// the provided [newState].
  bool hasStateChangedTo(YoutubePlayerValue newValue, PlayerState newState) {
    return playerState != newState && newValue.playerState == newState;
  }

  /// The percentage of the video that has been watched
  double get percentComplete =>
      position.inMicroseconds / videoLength.inMicroseconds;

  /// The timestamp to which the video has been buffered
  Duration get bufferedDuration =>
      Duration(microseconds: (videoLength.inMicroseconds * buffered).toInt());

  /// Whether or not the video time position is currently being updated
  bool isUpdatingTime = false;

  /// Whether or not the buffered amount is currently being updated
  bool isUpdatingBuffered = false;

  /// Whether or not the the video is seeking to a new time
  bool isSeeking = false;

  /// Whether or not the video video metadata has been added
  // TODO: wil this be weird with loading new videos? How to tell if new video has loaded new metadata?
  bool hasMetadata = false;

  /// Creates new [YoutubePlayerValue] with assigned parameters and overrides
  /// the old one.
  YoutubePlayerValue copyWith({
    bool? isReady,
    bool? hasPlayed,
    Duration? position,
    double? buffered,
    bool? isFullScreen,
    int? volume,
    PlayerState? playerState,
    double? playbackRate,
    String? playbackQuality,
    YoutubeError? error,
    YoutubeMetaData? metaData,
    bool? isUpdatingTime,
    bool? isUpdatingBuffered,
    bool? isSeeking,
    bool? hasMetadata,
    bool? isMuted,
    bool? isReplaying,
  }) {
    return YoutubePlayerValue(
      isReady: isReady ?? this.isReady,
      hasPlayed: hasPlayed ?? this.hasPlayed,
      position: position ?? this.position,
      buffered: buffered ?? this.buffered,
      isFullScreen: isFullScreen ?? this.isFullScreen,
      volume: volume ?? this.volume,
      playerState: playerState ?? this.playerState,
      playbackRate: playbackRate ?? this.playbackRate,
      playbackQuality: playbackQuality ?? this.playbackQuality,
      error: error ?? this.error,
      metaData: metaData ?? this.metaData,
      isUpdatingTime: isUpdatingTime ?? false, // if not specifically updating time, false
      isUpdatingBuffered: isUpdatingBuffered ?? false, // if not specifically updating buffered, false
      hasMetadata: hasMetadata ?? this.hasMetadata,
      isSeeking: isSeeking ?? false,
      isMuted: isMuted ?? this.isMuted,
      isReplaying: isReplaying ?? false,
    );
  }

  @override
  String toString() {
    return '$runtimeType('
        'metaData: ${metaData.toString()}, '
        'isReady: $isReady, '
        'position: $position, '
        'buffered: $buffered , '
        'volume: $volume, '
        'playerState: $playerState, '
        'playbackRate: $playbackRate, '
        'playbackQuality: $playbackQuality, '
        'isUpdatingTime: $isUpdatingTime, '
        'isUpdatingBuffered: $isUpdatingBuffered, '
        'hasMetadata: $hasMetadata, '
        'isSeeking: $isSeeking, '
        'isFullScreen: $isFullScreen, '
        'hasPlayed: $hasPlayed, '
        'error: $error, '
        'isMuted: $isMuted, '
        'isReplaying: $isReplaying'
        ')';
  }
}
