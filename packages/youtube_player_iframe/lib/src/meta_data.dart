// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// Meta data for Youtube Video.
class YoutubeMetaData {
  /// Youtube video ID of the currently loaded video.
  final String videoId;

  /// Video title of the currently loaded video.
  final String title;

  /// Channel name or uploader of the currently loaded video.
  final String author;

  /// Total duration of the currently loaded video.
  final Duration duration;

  /// Creates [YoutubeMetaData] for Youtube Video.
  const YoutubeMetaData({
    this.videoId = '',
    this.title = '',
    this.author = '',
    this.duration = const Duration(),
  });

  /// An empty [YoutubeMetaData].
  static const YoutubeMetaData empty = const YoutubeMetaData(
    videoId: '',
    title: '',
    author: '',
    duration: Duration.zero,
  );

  /// Whether or not this [YoutubeMetaData] is empty.
  bool get isEmpty => this == empty;

  /// Creates [YoutubeMetaData] from raw json video data.
  factory YoutubeMetaData.fromRawData(dynamic rawData) {
    final data = rawData as Map<String, dynamic>;
    final durationInMs = ((data['duration'] ?? 0).toDouble() * 1000).floor();
    return YoutubeMetaData(
      videoId: data['videoId'],
      title: data['title'],
      author: data['author'],
      duration: Duration(milliseconds: durationInMs),
    );
  }

  @override
  String toString() {
    return 'YoutubeMetaData('
        'videoId: $videoId, '
        'title: $title, '
        'author: $author, '
        'duration: $duration'
        ')';
  }

  @override
  bool operator ==(Object other) => other is YoutubeMetaData &&
      duration == other.duration &&
      videoId == other.videoId &&
      author == other.author &&
      title == other.title;

  @override
  int get hashCode => 17 * (
      duration.hashCode +
      videoId.hashCode +
      author.hashCode +
      title.hashCode
  );

}
