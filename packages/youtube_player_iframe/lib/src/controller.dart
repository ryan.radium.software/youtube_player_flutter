// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import 'package:youtube_player_iframe/extensions.dart';

import 'enums/player_state.dart';
import 'enums/playlist_type.dart';
import 'enums/thumbnail_quality.dart';
import 'enums/youtube_error.dart';
import 'meta_data.dart';
import 'player_params.dart';
import 'player_value.dart';

// ignore_for_file: avoid_catches_without_on_clauses

/// Controls a youtube player, and provides updates when the state is changing.
///
/// The video is displayed in a Flutter app by creating a [YoutubePlayerIFrame] widget.
///
/// After [YoutubePlayerController.close] all further calls are ignored.
class YoutubePlayerController {
  /// Creates [YoutubePlayerController].
  YoutubePlayerController({
    required this.initialVideoId,
    this.params = const YoutubePlayerParams(),
  }) {
    invokeJavascript = (_) async {};
  }

  YoutubePlayerController copyWith({
    String? initialVideoId,
    YoutubePlayerParams? params,
  }) => YoutubePlayerController(
    initialVideoId: initialVideoId ?? this.initialVideoId,
    params: params ?? this.params,
  );

  //todo: play pause timing glitch happens again if screen off

  /// The Youtube video id for initial video to be loaded.
  final String initialVideoId;

  /// Defines default parameters for the player.
  YoutubePlayerParams params;

  /// Can be used to invokes javascript function.
  ///
  /// Ensure that the player is ready before using this.
  late Future<dynamic> Function(String function) invokeJavascript;

  /// Called when player enters fullscreen.
  VoidCallback? onEnterFullscreen;

  /// Called when player exits fullscreen.
  VoidCallback? onExitFullscreen;

  final StreamController<YoutubePlayerValueImpl> _controller =
      StreamController.broadcast();

  late final YoutubePlayerValueImpl _value = YoutubePlayerValueImpl(
    onPlay: PlayerCallback(_onPlayEvents.call),
    onAnyEvent: PlayerCallback(_onAnyEvents.call),
    onPlaybackRateChanged: PlayerCallback(_onPlaybackRateChangedEvents.call),
    onError: PlayerCallback(_onErrorEvents.call),
    onBufferedChanged: PlayerCallback(_onBufferedUpdatedEvents.call),
    onBuffering: PlayerCallback(_onBufferingEvents.call),
    onEnd: PlayerCallback(_onEndEvents.call),
    // onFullScreenToggled: PlayerCallback.combine(),
    // onHasPlayed: PlayerCallback.combine(),
    onMetaDataUpdated: PlayerCallback(_onMetadataLoadedEvents.callAndClear),
    onMuteToggled: PlayerCallback(_onMuteToggledEvents.call),
    onPause: PlayerCallback(_onPauseEvents.call),
    onPlaybackQualityChanged: PlayerCallback(_onPlaybackQualityChangedEvents.call),
    onPositionChanged: PlayerCallback(_onTimeUpdatedEvents.call),
    // onReady: PlayerCallback.combine(),
    onVolumeChanged: PlayerCallback(_onVolumeChangedEvents.call),
    onSeek: PlayerCallback(_onSeekEvents.call),
  );

  /// The [YoutubePlayerValue].
  YoutubePlayerValue get value {
    return _value;
  }

  void _handleReady() {
    _value.isReady = true;
  }

  void _handlePlaybackQualityChange(List<Object?> args) {
    _value.setMultiple(
      playbackQuality: args.first as String,
      position: Duration(microseconds: ((args[1].as<num>() ?? 0) * 1000000).toInt()),
      buffered: (args.last as num).toDouble(),
    );
  }

  void _handlePlaybackRateChange(List<Object?> args) {
    var rate = (args.first as num).toDouble().toPlaybackRate();
    if (rate != _value.playbackRate) {
      setPlaybackRate(_value.playbackRate);
    }
    _value.setMultiple(
      position: Duration(microseconds: ((args[1].as<num>() ?? 0) * 1000000).toInt()),
      buffered: (args.last as num).toDouble(),
    );
  }

  void _handleErrors(List<Object?> args) {
    _value.setMultiple(
      error: (args.first as int).toYoutubeError(),
      position: Duration(microseconds: ((args[1].as<num>() ?? 0) * 1000000).toInt()),
      buffered: (args.last as num).toDouble(),
    );
  }

  void _handleStateChange(List<Object?> args) {
    final state = (args.first as int).toPlayerState();
    final position = Duration(microseconds: ((args[1].as<num>() ?? 0) * 1000000).toInt());
    _value.setMultiple(
      playerState: state,
      position: _value.isSeeking ? null : position,
      buffered: (args.last as num).toDouble(),
      isReady: state == PlayerState.unStarted ? true : null,
      hasPlayed: state == PlayerState.playing ? true : null,
      error: state == PlayerState.playing ? YoutubeError.none : null,
    );
    if (_value.playerState.isEnded() && params.repeat) {
      replay();
    }
  }

  void _handleVideoData(List<Object?> args) {
    _value.metaData = YoutubeMetaData.fromRawData(args.first);
  }

  void _handleVideoTime(List<Object?> args) {
    final position =
        Duration(microseconds: ((args.first as num) * 1000000).toInt());
    final buffered = args[1].as<num>()?.toDouble() ?? 0.0;

    if (params.strictStartAndEnd) {
      if (position < params.startAt) {
        seekTo(params.startAt);
        return;
      } else if (position > (params.endAt ?? _value.metaData.duration)) {
        if (params.repeat) {
          replay();
        } else {
          _value.setMultiple(
            position: params.startAt,
            buffered: buffered,
            playerState: PlayerState.ended,
          );
          pause();
          seekTo(params.startAt);
        }
        return;
      }
    }
    if (position >= (params.endAt ?? _value.metaData.duration)) {
      if (!_value.isSeeking) {
        if (params.repeat) {
          // print('TIMEUP: not seeking too far repeat $position, $buffered, ${params.endAt ?? _value.metaData.duration}');
          replay();
          return;
        } else {
          // print('TIMEUP: not seeking too far and no repeat $position, $buffered, ${params.endAt ?? _value.metaData.duration}');
          _value.setMultiple(
            position: params.startAt,
            buffered: buffered,
            playerState: PlayerState.ended,
          );
          pause();
          seekTo(params.startAt);
          return;
        }
      }
    }

    _value.setMultiple(
      position: position,
      buffered: buffered,
    );
  }

  /// Updates [YoutubePlayerController] with provided [data].
  ///
  /// Intended for internal usage only.
  void handle(String handlerName, [List<Object?> args = const []]) {
    // log('HANDLE $handlerName: $args\n\t$_value');
    switch (handlerName) {
      case YoutubeStateHandler.Ready:
        return _handleReady();
      case YoutubeStateHandler.PlaybackQualityChange:
        return _handlePlaybackQualityChange(args);
      case YoutubeStateHandler.PlaybackRateChange:
        return _handlePlaybackRateChange(args);
      case YoutubeStateHandler.Errors:
        return _handleErrors(args);
      case YoutubeStateHandler.StateChange:
        return _handleStateChange(args);
      case YoutubeStateHandler.VideoData:
        return _handleVideoData(args);
      case YoutubeStateHandler.VideoTime:
        return _handleVideoTime(args);
    }
  }

  final List<PlayerCallback> _onPauseEvents = [];
  final List<PlayerCallback> _onAnyEvents = [];
  final List<PlayerCallback> _onPlayEvents = [];
  final List<PlayerCallback> _onEndEvents = [];
  final List<PlayerCallback> _onBufferingEvents = [];
  final List<PlayerCallback> _onTimeUpdatedEvents = [];
  final List<PlayerCallback> _onBufferedUpdatedEvents = [];
  final List<PlayerCallback> _onMetadataLoadedEvents = [];
  final List<PlayerCallback> _onSeekEvents = [];
  final List<PlayerCallback> _onReplayEvents = [];
  final List<PlayerCallback> _onMuteToggledEvents = [];
  final List<PlayerCallback> _onVolumeChangedEvents = [];
  final List<PlayerCallback> _onPlaybackRateChangedEvents = [];
  final List<PlayerCallback> _onPlaybackQualityChangedEvents = [];
  final List<PlayerCallback> _onErrorEvents = [];

  void _callCallbacks(List<PlayerCallback> callbacks) {
    if (callbacks.isEmpty) return;
    callbacks.removeWhere((callback) => callback.isCanceled);
    callbacks.where((it) => !it.isPaused).forEach((it) => it.action(_value));
  }

  void _callOnReplayCallbacks() =>  _callCallbacks(_onReplayEvents);

  /// Adds a callback to be called when the video is paused.
  PlayerCallback onVideoPause(
    void Function(YoutubePlayerValue event) onPlayerPause,
  ) {
    var callback = PlayerCallback(onPlayerPause);
    _onPauseEvents.add(callback);
    return callback;
  }

  /// Adds a callback to be called when the player's time is updated.
  /// See: [_callOnTimeUpdatedCallbacks]
  PlayerCallback onPlayerTimeUpdated(
    void Function(YoutubePlayerValue event) onPlayerTimeUpdated,
  ) {
    var callback = PlayerCallback(onPlayerTimeUpdated);
    _onTimeUpdatedEvents.add(callback);
    return callback;
  }

  /// Adds a callback to be called when the video starts playing.
  PlayerCallback onVideoPlay(
    void Function(YoutubePlayerValue event) onPlayerPlay,
  ) {
    var callback = PlayerCallback(onPlayerPlay);
    _onPlayEvents.add(callback);
    return callback;
  }

  /// Adds a callback to be called when the video ends.
  PlayerCallback onVideoEnd(
    void Function(YoutubePlayerValue event) onVideoEnd,
  ) {
    var callback = PlayerCallback(onVideoEnd);
    _onEndEvents.add(callback);
    return callback;
  }

  /// Adds a callback to be called when the video pauses for buffering.
  PlayerCallback onVideoBuffering(
    void Function(YoutubePlayerValue event) onVideoBuffering,
  ) {
    var callback = PlayerCallback(onVideoBuffering);
    _onBufferingEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when any event occurs in the player.
  PlayerCallback onAnyEvent(
      void Function(YoutubePlayerValue event) onAnyEvent,
  ) {
    var callback = PlayerCallback(onAnyEvent);
    _onAnyEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when metadata has loaded.
  PlayerCallback onMetadataLoaded(
      void Function(YoutubePlayerValue event) onMetadataLoaded,
  ) {
    var callback = PlayerCallback(onMetadataLoaded);
    _onMetadataLoadedEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when a Youtube error occurs.
  PlayerCallback onError(
      void Function(YoutubePlayerValue event) onError,
  ) {
    var callback = PlayerCallback(onError);
    _onErrorEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when metadata has loaded.
  PlayerCallback onSeek(
      void Function(YoutubePlayerValue event) onSeek,
  ) {
    var callback = PlayerCallback(onSeek);
    _onSeekEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when the video has been muted / unmuted.
  PlayerCallback onMuteToggled(
      void Function(YoutubePlayerValue event) onMuteToggled,
  ) {
    var callback = PlayerCallback(onMuteToggled);
    _onMuteToggledEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when the video has been muted / unmuted.
  PlayerCallback onVolumeChanged(
      void Function(YoutubePlayerValue event) onVolumeChanged,
  ) {
    var callback = PlayerCallback(onVolumeChanged);
    _onVolumeChangedEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when the video has been muted / unmuted.
  PlayerCallback onPlaybackRateChanged(
      void Function(YoutubePlayerValue event) onPlaybackRateChanged,
  ) {
    var callback = PlayerCallback(onPlaybackRateChanged);
    _onPlaybackRateChangedEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when the video has been muted / unmuted.
  PlayerCallback onPlaybackQualityChanged(
      void Function(YoutubePlayerValue event) onPlaybackQualityChanged,
  ) {
    var callback = PlayerCallback(onPlaybackQualityChanged);
    _onPlaybackQualityChangedEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when the video has started replaying.
  PlayerCallback onReplay(
      void Function(YoutubePlayerValue event) onReplay,
  ) {
    var callback = PlayerCallback(onReplay);
    _onReplayEvents.add(callback);
    return callback;
  }

  /// Add a callback to be called when amount of the video buffered changes.
  PlayerCallback onBufferedAmountChanged(
      void Function(YoutubePlayerValue event) onBufferedAmountChanged,
  ) {
    var callback = PlayerCallback(onBufferedAmountChanged);
    _onBufferedUpdatedEvents.add(callback);
    return callback;
  }

  /// Closes [YoutubePlayerController].
  ///
  /// Call when the controller is no longer used.
  Future<void> close() => _controller.close();

  /// Plays the currently cued/loaded video.
  ///
  /// The final player state after this function executes will be [PlayerState.playing].
  ///
  /// Note: A playback only counts toward a video's official view count if it is initiated via a native play button in the player.
  void play() => invokeJavascript('play()');

  /// Pauses the currently playing video.
  ///
  /// The final player state after this function executes will be [PlayerState.paused] unless the player is in the [PlayerState.ended] state when the function is called,
  /// in which case the player state will not change.
  void pause() => invokeJavascript('pause()');

  /// Stops and cancels loading of the current video.
  ///
  /// This function should be reserved for rare situations when you know that the user will not be watching additional video in the player.
  /// If your intent is to pause the video, you should just call the [YoutubePlayerController.pause] function.
  /// If you want to change the video that the player is playing, you can call one of the queueing functions without calling [YoutubePlayerController.stop] first.
  void stop() => invokeJavascript('stop()');

  /// This function loads and plays the next video in the playlist.
  ///
  /// If called while the last video in the playlist is being watched, and the playlist is set to play continuously (i.e. [YoutubePlayerParams.loop] is true),
  /// then the player will load and play the first video in the list, otherwise,
  /// the playback will end.
  void nextVideo() => invokeJavascript('next()');

  /// This function loads and plays the previous video in the playlist.
  ///
  /// If called while the last video in the playlist is being watched, and the playlist is set to play continuously (i.e. [YoutubePlayerParams.loop] is true),
  /// then the player will load and play the first video in the list, otherwise,
  /// the playback will end.
  void previousVideo() => invokeJavascript('previous()');

  /// This function loads and plays the specified video in the playlist.
  ///
  /// The [index] parameter specifies the index of the video that you want to play in the playlist.
  /// The parameter uses a zero-based index, so a value of 0 identifies the first video in the list.
  /// If you have shuffled the playlist, this function will play the video at the specified position in the shuffled playlist.
  void playVideoAt(int index) => invokeJavascript('playVideoAt($index)');

  /// This function loads and plays the specified video.
  ///
  /// [videoId] specifies the YouTube Video ID of the video to be played.
  /// In the YouTube Data API, a video resource's id property specifies the ID.
  ///
  /// [startAt] & [endAt] parameter accepts a [Duration].
  /// If specified, then the video will (start from the closest keyframe to the specified time / end at the specified time).
  void load(String videoId, {Duration startAt = Duration.zero, Duration? endAt}) {
    var loadParams = 'videoId:"$videoId",startSeconds:${startAt.inSeconds}';
    if (endAt != null && endAt > startAt) {
      loadParams += ',endSeconds:${endAt.inSeconds}';
    }
    _updateId(videoId);
    if (_value.hasError) {
      pause();
    } else {
      invokeJavascript('loadById({$loadParams})');
    }
  }

  /// This function loads the specified video's thumbnail and prepares the player to play the video.
  /// The player does not request the FLV until [YoutubePlayerController.play] or [YoutubePlayerController.seekTo] is called.
  ///
  /// [videoId] parameter specifies the YouTube Video ID of the video to be played.
  /// In the YouTube Data API, a video resource's id property specifies the ID.
  ///
  /// [startAt] & [endAt] parameter accepts a [Duration].
  /// If specified, then the video will (start from the closest keyframe to the specified time / end at the specified time).
  void cue(String videoId, {Duration startAt = Duration.zero, Duration? endAt}) {
    var cueParams = 'videoId:"$videoId",startSeconds:${startAt.inSeconds}';
    if (endAt != null && endAt > startAt) {
      cueParams += ',endSeconds:${endAt.inSeconds}';
    }
    _updateId(videoId);
    if (_value.hasError) {
      pause();
    } else {
      invokeJavascript('cueById({$cueParams})');
    }
  }

  /// This function loads the specified list and plays it.
  /// The list can be a playlist, a search results feed, or a user's uploaded videos feed.
  ///
  /// [list] contains a key that identifies the particular list of videos that YouTube should return.
  /// [listType] specifies the type of results feed that you are retrieving.
  /// [startAt] accepts a [Duration] and specifies the time from which the first video in the list should start playing.
  /// [index] specifies the index of the first video in the list that will play.
  /// The parameter uses a zero-based index, and the default parameter value is 0,
  /// so the default behavior is to load and play the first video in the list.
  void loadPlaylist(
    String list, {
    String listType = PlaylistType.playlist,
    int startAt = 0,
    int index = 0,
  }) {
    var loadParams =
        'list:"$list",listType:"$listType",index:$index,startSeconds:$startAt';
    invokeJavascript('loadPlaylist({$loadParams})');
  }

  /// Queues the specified list of videos.
  /// The list can be a playlist, a search results feed, or a user's uploaded videos feed.
  /// When the list is cued and ready to play, the player will broadcast a video cued event [PlayerState.cued].
  ///
  /// [list] contains a key that identifies the particular list of videos that YouTube should return.
  /// [listType] specifies the type of results feed that you are retrieving.
  /// [startAt] accepts a [Duration] and specifies the time from which the first video in the list should start playing.
  /// [index] specifies the index of the first video in the list that will play.
  /// The parameter uses a zero-based index, and the default parameter value is 0,
  /// so the default behavior is to load and play the first video in the list.
  void cuePlaylist(
    String list, {
    String listType = PlaylistType.playlist,
    int startAt = 0,
    int index = 0,
  }) {
    var cueParams =
        'list:"$list",listType:"$listType",index:$index,startSeconds:$startAt';
    invokeJavascript('cuePlaylist({$cueParams})');
  }

  void _updateId(String id) {
    if (id.length != 11) {
      _value.error = YoutubeError.invalidParam;
    } else {
      _value.setMultiple(error: YoutubeError.none, hasPlayed: false);
    }
  }

  // todo: maybe we can get other player info on every call like this?
  /// Mutes the player.
  void mute() => invokeJavascript('mute()')
      .then((_) => _value.isMuted = true);

  /// Unmutes the player.
  void unMute() => invokeJavascript('unMute()')
      .then((_) => _value.isMuted = false);

  /// Sets the volume of player.
  /// Max = 100 , Min = 0
  void setVolume(int volume) => volume >= 0 && volume <= 100
      ? invokeJavascript('setVolume($volume)')
      : throw Exception("Volume should be between 0 and 100");

  /// Seeks to a specified time in the video.
  ///
  /// If the player is paused when the function is called, it will remain paused.
  /// If the function is called from another state (playing, video cued, etc.), the player will play the video.
  ///
  /// [allowSeekAhead] determines whether the player will make a new request to the server
  /// if the seconds parameter specifies a time outside of the currently buffered video data.
  ///
  /// Default allowSeekAhead = true
  void seekTo(Duration position, {bool allowSeekAhead = true}) {
    var _position = position;
    if (params.strictStartAndEnd) {
      if (_position < params.startAt) {
        _position = params.startAt;
      } else if (_position > (params.endAt ?? _value.metaData.duration)) {
        if (params.repeat) {
          _position = params.startAt;
        } else {
          _position = params.endAt ?? _value.metaData.duration;
        }
      }
    }
    // todo: not stop here? Also commas?
    invokeJavascript('stopSendCurrentTimeInterval()').then((_) => {
      _value.setMultiple(
        position: _position,
        isSeeking: true,       // todo: maybe move this to controller??
      ),
      invokeJavascript('seekTo(${_position.inMicroseconds / 1000000},$allowSeekAhead)')
    });

  }

  /// Replays the video, starting at the [startAt] time.
  void replay() {
    seekTo(params.startAt);
    _callOnReplayCallbacks();
    // log('replay before play');
    play();
    // log('replay after play');
  }

  /// Sets the size in pixels of the player.
  void setSize(Size size) =>
      invokeJavascript('setSize(${size.width}, ${size.height})');

  /// Sets the playback speed for the video.
  void setPlaybackRate(PlaybackRate rate) =>
      invokeJavascript('setPlaybackRate(${rate.toDouble()})').then(
          (value) => _value.playbackRate = rate
      );

  /// This function indicates whether the video player should continuously play a playlist
  /// or if it should stop playing after the last video in the playlist ends.
  ///
  /// The default behavior is that playlists do not loop.
  // ignore: avoid_positional_boolean_parameters
  void setLoop(bool loop) => invokeJavascript('setLoop($loop)');

  /// This function indicates whether a playlist's videos should be shuffled so that they play back in an order different from the one that the playlist creator designated.
  ///
  /// If you shuffle a playlist after it has already started playing, the list will be reordered while the video that is playing continues to play.
  /// The next video that plays will then be selected based on the reordered list.
  // ignore: avoid_positional_boolean_parameters
  void setShuffle(bool shuffle) => invokeJavascript('setShuffle($shuffle)');

  /// Hides top menu i.e. title, playlist, share icon shown at top of the player.
  ///
  /// Might violates Youtube's TOS. Use at your own risk.
  void hideTopMenu() => invokeJavascript('hideTopMenu()');

  /// Hides pause overlay i.e. related videos shown when player is paused.
  ///
  /// Might violates Youtube's TOS. Use at your own risk.
  void hidePauseOverlay() => invokeJavascript('hidePauseOverlay()');

  /// MetaData for the currently loaded or cued video.
  YoutubeMetaData get metadata => _value.metaData;

  /// Resets the value of [YoutubePlayerController].
  void reset() => _value.setMultiple(
    isReady: false,
    isFullScreen: false,
    playerState: PlayerState.unknown,
    hasPlayed: false,
    position: Duration.zero,
    buffered: 0.0,
    error: YoutubeError.none,
    metaData: YoutubeMetaData.empty,
  );

  /// Converts fully qualified YouTube Url to video id.
  ///
  /// If videoId is passed as url then no conversion is done.
  static String? convertUrlToId(String url, {bool trimWhitespaces = true}) {
    if (!url.contains("http") && (url.length == 11)) return url;
    if (trimWhitespaces) url = url.trim();

    for (var regex in [
      r'^https:\/\/(?:www\.|m\.)?youtube\.com\/watch\?v=([_\-a-zA-Z0-9]{11}).*$',
      r'^https:\/\/(?:www\.|m\.)?youtube(?:-nocookie)?\.com\/embed\/([_\-a-zA-Z0-9]{11}).*$',
      r'^https:\/\/youtu\.be\/([_\-a-zA-Z0-9]{11}).*$',
    ]) {
      Match? match = RegExp(regex).firstMatch(url);
      if (match != null && match.groupCount >= 1) return match.group(1);
    }

    return null;
  }

  /// Grabs YouTube video's thumbnail for provided video id.
  ///
  /// If [webp] is true, webp version of the thumbnail will be retrieved,
  /// Otherwise a JPG thumbnail.
  static String getThumbnail({
    required String videoId,
    String quality = ThumbnailQuality.standard,
    bool webp = true,
  }) {
    return webp
        ? 'https://i3.ytimg.com/vi_webp/$videoId/$quality.webp'
        : 'https://i3.ytimg.com/vi/$videoId/$quality.jpg';
  }
}

///
class YoutubeStateHandler {
  static const String Ready = "Ready";
  static const String StateChange = "StateChange";
  static const String PlaybackQualityChange = "PlaybackQualityChange";
  static const String PlaybackRateChange = "PlaybackRateChange";
  static const String Errors = "Errors";
  static const String VideoData = "VideoData";
  static const String VideoTime = "VideoTime";
}