// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import 'callback.dart';
import 'enums/playback_rate.dart';
import 'enums/player_state.dart';
import 'enums/youtube_error.dart';
import 'meta_data.dart';

/// Youtube Player value
abstract class YoutubePlayerValue {
  /// Returns true when the player is ready to play videos.
  abstract final bool isReady;

  /// Returns true once the video start playing for the first time.
  abstract final bool hasPlayed;

  /// The current position of the video.
  abstract final Duration position;

  /// The percent up to which the video is buffered.
  abstract final double buffered;

  /// Reports true if video is fullscreen.
  abstract final bool isFullScreen;

  /// The current volume assigned for the player.
  abstract final int volume;

  /// The current state of the player defined as [PlayerState].
  abstract final PlayerState playerState;

  /// The current video playback rate defined as [PlaybackRate].
  abstract final PlaybackRate playbackRate;

  /// Reports the error code as described
  /// [here](https://developers.google.com/youtube/iframe_api_reference#Events).
  ///
  /// See the onError Section.
  abstract final YoutubeError error;

  /// Returns true is player has errors.
  bool get hasError => error != YoutubeError.none;

  /// Reports the current playback quality.
  abstract final String? playbackQuality;

  /// Returns meta data of the currently loaded/cued video.
  abstract final YoutubeMetaData metaData;

  /// Returns whether or not the video has been muted.
  abstract final bool isMuted;

  Duration get videoLength => metaData.duration;

  String get videoTitle => metaData.title;

  String get videoAuthor => metaData.author;

  String get videoId => metaData.videoId;

  /// The percentage of the video that has been watched
  double get percentComplete =>
      position.inMicroseconds / videoLength.inMicroseconds;

  /// The timestamp to which the video has been buffered
  Duration get bufferedDuration =>
      Duration(microseconds: (videoLength.inMicroseconds * buffered).toInt());

  /// Whether or not the video video metadata has been added
  // TODO: wil this be weird with loading new videos? How to tell if new video has loaded new metadata?
  bool get hasMetadata => !metaData.isEmpty;
}

/// Youtube Player value implementation. Setting any of the fields on this
/// object will call the relevant callbacks related to the change. If you are
/// setting multiple fields sequentially, be sure to use [setMultiple]. This
/// ensures that the proper callbacks are called and that the value with all
/// of the correct updates is sent to the callbacks.
class YoutubePlayerValueImpl extends YoutubePlayerValue {
  /// The duration, current position, buffering state, error state and settings
  /// of a [YoutubePlayerController].
  YoutubePlayerValueImpl({
    bool isReady = false,
    bool hasPlayed = false,
    Duration position = const Duration(),
    double buffered = 0.0,
    bool isFullScreen = false,
    int volume = 100,
    PlayerState playerState = PlayerState.unknown,
    PlaybackRate playbackRate = PlaybackRate.normal,
    String? playbackQuality,
    error = YoutubeError.none,
    YoutubeMetaData metaData = YoutubeMetaData.empty,
    bool isUpdatingBuffered = false,
    bool isSeeking = false,
    bool isMuted = false,
    PlayerCallback? onReady,
    PlayerCallback? onHasPlayed,
    PlayerCallback? onPositionChanged,
    PlayerCallback? onBufferedChanged,
    PlayerCallback? onFullScreenToggled,
    PlayerCallback? onVolumeChanged,
    PlayerCallback? onPlay,
    PlayerCallback? onPause,
    PlayerCallback? onEnd,
    PlayerCallback? onBuffering,
    PlayerCallback? onPlaybackRateChanged,
    PlayerCallback? onPlaybackQualityChanged,
    PlayerCallback? onError,
    PlayerCallback? onMetaDataUpdated,
    PlayerCallback? onMuteToggled,
    PlayerCallback? onAnyEvent,
    PlayerCallback? onSeek,
  }) :
    _isReady = isReady,
    _hasPlayed = hasPlayed,
    _position = position,
    _buffered = buffered,
    _isFullScreen = isFullScreen,
    _volume = volume,
    _playerState = playerState,
    _playbackRate = playbackRate,
    _playbackQuality = playbackQuality,
    _error = error,
    _metaData = metaData,
    _isSeeking = isSeeking,
    _isMuted = isMuted,

    _onReady = onReady,
    _onHasPlayed = onHasPlayed,
    _onPositionChanged = onPositionChanged,
    _onBufferedChanged = onBufferedChanged,
    _onFullScreenToggled = onFullScreenToggled,
    _onVolumeChanged = onVolumeChanged,
    _onPlaybackRateChanged = onPlaybackRateChanged,
    _onPlaybackQualityChanged = onPlaybackQualityChanged,
    _onError = onError,
    _onMetaDataUpdated = onMetaDataUpdated,
    _onMuteToggled = onMuteToggled,
    _onAnyEvent = onAnyEvent,
    _onPlay = onPlay,
    _onPause = onPause,
    _onEnd = onEnd,
    _onBuffering = onBuffering,
    _onSeek = onSeek;

  final PlayerCallback? _onReady;
  final PlayerCallback? _onHasPlayed;
  final PlayerCallback? _onPositionChanged;
  final PlayerCallback? _onBufferedChanged;
  final PlayerCallback? _onFullScreenToggled;
  final PlayerCallback? _onVolumeChanged;
  final PlayerCallback? _onPlaybackRateChanged;
  final PlayerCallback? _onPlaybackQualityChanged;
  final PlayerCallback? _onError;
  final PlayerCallback? _onMetaDataUpdated;
  final PlayerCallback? _onMuteToggled;
  final PlayerCallback? _onAnyEvent;
  final PlayerCallback? _onPlay;
  final PlayerCallback? _onPause;
  final PlayerCallback? _onEnd;
  final PlayerCallback? _onBuffering;
  final PlayerCallback? _onSeek;

  bool _isReady;
  /// Returns true when the player is ready to play videos.
  bool get isReady => _isReady;
  set isReady(bool isReady) {
    if (_isReady == isReady) {
      return;
    }
    _isReady = isReady;
    _onReady?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  bool _hasPlayed;
  /// Returns true once the video start playing for the first time.
  bool get hasPlayed => _hasPlayed;
  set hasPlayed(bool hasPlayed) {
    if (_hasPlayed == hasPlayed) {
      return;
    }
    _hasPlayed = hasPlayed;
    _onHasPlayed?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  Duration _position;
  /// The current position of the video.
  Duration get position => _position;
  set position(Duration position) {
    if (_position == position) {
      return;
    }
    _position = position;
    _onPositionChanged?.action.call(this);
    if (isSeeking) {
      isSeeking = false;
      _onSeek?.action.call(this);
    }
    _onAnyEvent?.action.call(this);
  }

  double _buffered;
  /// The percent up to which the video is buffered.
  double get buffered => _buffered;
  set buffered(double buffered) {
    if (_buffered == buffered) {
      return;
    }
    _buffered = buffered;
    _onBufferedChanged?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  bool _isFullScreen;
  /// Reports true if video is fullscreen.
  bool get isFullScreen => _isFullScreen;
  set isFullScreen(bool isFullScreen) {
    if (_isFullScreen == isFullScreen) {
      return;
    }
    _isFullScreen = isFullScreen;
    _onFullScreenToggled?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  int _volume;
  /// The current volume assigned for the player.
  int get volume => _volume;
  set volume(int volume) {
    if (_volume == volume) {
      return;
    }
    _volume = volume;
    _onVolumeChanged?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  PlayerState _playerState;
  /// The current state of the player defined as [PlayerState].
  PlayerState get playerState => _playerState;
  set playerState(PlayerState newState) {
    if (_playerState == newState) {
      return;
    }
    var oldState = _playerState;
    _playerState = newState;
    if (oldState.isNotPlaying() && newState.isPlaying()) {
      _onPlay?.action.call(this);
    } else if (oldState.isNotPaused() && newState.isPaused()) {
      _onPause?.action.call(this);
    } else if (oldState.isNotBuffering() && newState.isBuffering()) {
      _onBuffering?.action.call(this);
    } else if (oldState.isNotEnded() && newState.isEnded()) {
      _onEnd?.action.call(this);
    }
    _onAnyEvent?.action.call(this);
  }

  PlaybackRate _playbackRate;
  /// The current video playback rate defined as [PlaybackRate].
  PlaybackRate get playbackRate => _playbackRate;
  set playbackRate(PlaybackRate playbackRate) {
    if (_playbackRate == playbackRate) {
      return;
    }
    _playbackRate = playbackRate;
    _onPlaybackRateChanged?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  YoutubeError _error;
  /// Reports the error code as described
  /// [here](https://developers.google.com/youtube/iframe_api_reference#Events).
  ///
  /// See the onError Section.
  YoutubeError get error => _error;
  set error(YoutubeError error) {
    if (_error == error) {
      return;
    }
    _error = error;
    _onError?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  String? _playbackQuality;
  /// Reports the current playback quality.
  String? get playbackQuality => _playbackQuality;
  set playbackQuality(String? playbackQuality) {
    if (_playbackQuality == playbackQuality) {
      return;
    }
    _playbackQuality = playbackQuality;
    _onPlaybackQualityChanged?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  YoutubeMetaData _metaData;
  /// Returns meta data of the currently loaded/cued video.
  YoutubeMetaData get metaData => _metaData;
  set metaData(YoutubeMetaData metaData) {
    if (_metaData == metaData || metaData.isEmpty) {
      return;
    }
    _metaData = metaData;
    _onMetaDataUpdated?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  bool _isMuted;
  /// Returns whether or not the video has been muted.
  bool get isMuted => _isMuted;
  set isMuted(bool isMuted) {
    if (_isMuted == isMuted) {
      return;
    }
    _isMuted = isMuted;
    _onMuteToggled?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  /// Whether or not the video time position is currently being updated
  bool isUpdatingTime = false;

  /// Whether or not the buffered amount is currently being updated
  bool isUpdatingBuffered = false;

  bool _isSeeking;
  /// Whether or not the the video is seeking to a new time
  bool get isSeeking => _isSeeking;
  set isSeeking(bool isSeeking) {
    if (_isSeeking == isSeeking) {
      return;
    }
    _isSeeking = isSeeking;
    _onSeek?.action.call(this);
    _onAnyEvent?.action.call(this);
  }

  ///
  bool isEnding = false;

  /// Use this for batch setting multiple fields at once. This is useful for
  /// when multiple things have changed at once, as callbacks will properly
  /// include all new information.
  void setMultiple({
    bool? isReady,
    bool? hasPlayed,
    Duration? position,
    double? buffered,
    bool? isFullScreen,
    int? volume,
    PlayerState? playerState,
    PlaybackRate? playbackRate,
    String? playbackQuality,
    YoutubeError? error,
    YoutubeMetaData? metaData,
    bool? isMuted,
    bool? isSeeking,
  }) {
    // cache all old states
    var ogIsReady = _isReady;
    var ogHasPlayed = _hasPlayed; //////////////////// todo: ////////
    var ogPosition = _position;
    var ogBuffered = _buffered;
    var ogIsFullScreen = _isFullScreen;
    var ogVolume = _volume;
    var ogPlayerState = _playerState;
    var ogPlaybackRate = _playbackRate;
    var ogPlaybackQuality = _playbackQuality;
    var ogError = _error;
    var ogMetaData = _metaData;
    var ogIsMuted = _isMuted;
    var ogIsSeeking = _isSeeking;
    // set new states
    if (isReady != null) _isReady = isReady;
    if (hasPlayed != null) _hasPlayed = hasPlayed; //////////////////// todo: ////////
    if (position != null) _position = position;
    if (buffered != null) _buffered = buffered;
    if (isFullScreen != null) _isFullScreen = isFullScreen;
    if (volume != null) _volume = volume;
    if (playerState != null) _playerState = playerState;
    if (playbackRate != null) _playbackRate = playbackRate;
    if (playbackQuality != null) _playbackQuality = playbackQuality;
    if (error != null) _error = error;
    if (metaData != null) _metaData = metaData;
    if (isMuted != null) _isMuted = isMuted;
    if (isSeeking != null) _isSeeking = isSeeking;
    // perform callbacks if states have changed
    var anyEvent = false;
    if (isReady != null && isReady != ogIsReady) {
      _onReady?.action.call(this);
      anyEvent = true;
    }
    if (position != null && position != ogPosition) {
      _onPositionChanged?.action.call(this);
      if (_isSeeking) {
        _isSeeking = false;
        _onSeek?.action.call(this);
      }
      anyEvent = true;
    }
    if (buffered != null && buffered != ogBuffered) {
      _onBufferedChanged?.action.call(this);
      anyEvent = true;
    }
    if (isFullScreen != null && isFullScreen != ogIsFullScreen) {
      _onFullScreenToggled?.action.call(this);
      anyEvent = true;
    }
    if (volume != null && volume != ogVolume) {
      _onVolumeChanged?.action.call(this);
      anyEvent = true;
    }
    if (playbackRate != null && playbackRate != ogPlaybackRate) {
      _onPlaybackRateChanged?.action.call(this);
      anyEvent = true;
    }
    if (playbackQuality != null && playbackQuality != ogPlaybackQuality) {
      _onPlaybackQualityChanged?.action.call(this);
      anyEvent = true;
    }
    if (error != null && error != ogError) {
      _onError?.action.call(this);
      anyEvent = true;
    }
    if (metaData != null && metaData != ogMetaData) {
      _onMetaDataUpdated?.action.call(this);
      anyEvent = true;
    }
    if (isMuted != null && isMuted != ogIsMuted) {
      _onMuteToggled?.action.call(this);
      anyEvent = true;
    }
    if (isSeeking != null && isSeeking != ogIsSeeking) {
      _onSeek?.action.call(this);
      anyEvent = true;
    }
    if (playerState != null && playerState != ogPlayerState) {
      if (ogPlayerState.isNotPlaying() && playerState.isPlaying()) {
        _onPlay?.action.call(this);
      } else if (ogPlayerState.isNotPaused() && playerState.isPaused()) {
        _onPause?.action.call(this);
      } else if (ogPlayerState.isNotBuffering() && playerState.isBuffering()) {
        _onBuffering?.action.call(this);
      } else if (ogPlayerState.isNotEnded() && playerState.isEnded()) {
        _onEnd?.action.call(this);
      }
      anyEvent = true;
    }
    if (anyEvent) {
      _onAnyEvent?.action.call(this);
    }
  }

  @override
  String toString() {
    return
      '$runtimeType('
        'metaData: ${_metaData.toString()}, '
        'isReady: $_isReady, '
        'position: $_position, '
        'buffered: $_buffered , '
        'volume: $_volume, '
        'playerState: $_playerState, '
        'playbackRate: $_playbackRate, '
        'playbackQuality: $_playbackQuality, '
        'hasMetadata: $hasMetadata, '
        'isSeeking: $_isSeeking, '
        'isFullScreen: $_isFullScreen, '
        'hasPlayed: $_hasPlayed, '
        'error: $_error, '
        'isSeeking: $_isSeeking, '
        'isMuted: $_isMuted'
      ')';
  }
}

/// A [Callback] for listening to the YouTube player.
class PlayerCallback extends Callback {
  @override final void Function(YoutubePlayerValue event) _action;
  @override final void Function(
      YoutubePlayerValue event,
      Error error,
      StackTrace stackTrace)? onError;
  @override final void Function(
      YoutubePlayerValue event,
      Exception error,
      StackTrace stackTrace)? onException;
  @override final void Function(
      YoutubePlayerValue event,
      dynamic exceptionOrError,
      StackTrace stackTrace)? onExceptionOrError;

  /// Creates a [PlayerCallback]. Takes a [Function] with a [YoutubePlayerValue]
  /// as the action.
  PlayerCallback(this._action,
      {this.onError, this.onException, this.onExceptionOrError}
      ) : super(
      _action,
      onError: onError,
      onException: onException,
      onExceptionOrError: onExceptionOrError
  );

  PlayerCallback.combine(List<PlayerCallback> callbacks) : this(
    (event) {
      for (var it in callbacks) {
        it.action.call(event);
      }
    },
    onError: (event, error, stacktrace) {
      for (var it in callbacks) {
        it.onError?.call(event, error, stacktrace);
      }
    },
    onException: (event, error, stacktrace) {
      for (var it in callbacks) {
        it.onException?.call(event, error, stacktrace);
      }
    },
    onExceptionOrError: (event, error, stacktrace) {
      for (var it in callbacks) {
        it.onExceptionOrError?.call(event, error, stacktrace);
      }
    },
  );


  ///
  @override void Function(YoutubePlayerValue event) get action => (event) {
    try {
      _action.call(event);
    } on Exception catch(exception, stackTrace) {
      onException?.call(event, exception, stackTrace);
      onExceptionOrError?.call(event, exception, stackTrace);
      if (onException == null && onExceptionOrError == null) rethrow;
    } on Error catch(error, stackTrace) {
      onError?.call(event, error, stackTrace);
      onExceptionOrError?.call(event, error, stackTrace);
      if (onException == null && onExceptionOrError == null) rethrow;
    }
  };

}

extension PlayerCallbackListExtensions on List<PlayerCallback> {
  void call(YoutubePlayerValue value) {
    if (isEmpty) return;
    removeWhere((callback) => callback.isCanceled);
    where((it) => !it.isPaused).forEach((it) => it.action(value));
  }
  void callAndClear(YoutubePlayerValue value) {
    call(value);
    clear();
  }
  void cancel() => forEach((it) { it.cancel(); });
}