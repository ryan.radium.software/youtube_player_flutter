// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// Playback rate or speed for the video.
///
/// Find more about it [here](https://developers.google.com/youtube/iframe_api_reference#getPlaybackRate).
class DEPRECATEDPlaybackRate {
  /// Sets playback rate to 2.0 times.
  static const double twice = 2.0;

  /// Sets playback rate to 1.75 times.
  static const double oneAndAThreeQuarter = 1.75;

  /// Sets playback rate to 1.5 times.
  static const double oneAndAHalf = 1.5;

  /// Sets playback rate to 1.25 times.
  static const double oneAndAQuarter = 1.25;

  /// Sets playback rate to 1.0 times.
  static const double normal = 1.0;

  /// Sets playback rate to 0.75 times.
  static const double threeQuarter = 0.75;

  /// Sets playback rate to 0.5 times.
  static const double half = 0.5;

  /// Sets playback rate to 0.25 times.
  static const double quarter = 0.25;

  /// All
  static const List<double> all = [
    twice,
    oneAndAThreeQuarter,
    oneAndAHalf,
    oneAndAQuarter,
    normal,
    threeQuarter,
    half,
    quarter,
  ];
}

/// Playback rate or speed for the video.
///
/// Find more about it [here](https://developers.google.com/youtube/iframe_api_reference#getPlaybackRate).
enum PlaybackRate {
  /// Sets playback rate to 2.0 times.
  double,
  /// Sets playback rate to 1.75 times.
  oneAndAThreeQuarters,
  /// Sets playback rate to 1.5 times.
  oneAndAHalf,
  /// Sets playback rate to 1.25 times.
  oneAndAQuarter,
  /// Sets playback rate to 1.0 times.
  normal,
  /// Sets playback rate to 0.75 times.
  threeQuarters,
  /// Sets playback rate to 0.5 times.
  half,
  /// Sets playback rate to 0.25 times.
  quarter,
}

///
extension DoublePlaybackRateExtensions on double {
  ///
  PlaybackRate toPlaybackRate() {
    if (this == 2.0) {
      return PlaybackRate.double;
    } else if (this == 1.75) {
      return PlaybackRate.oneAndAThreeQuarters;
    } else if (this == 1.5) {
      return PlaybackRate.oneAndAHalf;
    } else if (this == 1.25) {
      return PlaybackRate.oneAndAQuarter;
    } else if (this == 1.0) {
      return PlaybackRate.normal;
    } else if (this == 0.75) {
      return PlaybackRate.threeQuarters;
    } else if (this == 0.5) {
      return PlaybackRate.half;
    } else if (this == 0.25) {
      return PlaybackRate.quarter;
    }
    throw ArgumentError(
        "A PlaybackRate must be one of: "
            "${PlaybackRate.values.reversed.map((e) => e.toDouble())}. "
        "Rate provided: "
            "$this."
    );
  }
}

///
extension PlaybackRateExtensions on PlaybackRate {
  ///
  double toDouble() {
    switch (this) {
      case PlaybackRate.double:
        return 2.0;
      case PlaybackRate.oneAndAThreeQuarters:
        return 1.75;
      case PlaybackRate.oneAndAHalf:
        return 1.5;
      case PlaybackRate.oneAndAQuarter:
        return 1.25;
      case PlaybackRate.normal:
        return 1.0;
      case PlaybackRate.threeQuarters:
        return 0.75;
      case PlaybackRate.half:
        return 0.5;
      case PlaybackRate.quarter:
        return 0.25;
    }
  }

  /// Get the next higher [PlaybackRate], looping back around.
  PlaybackRate next({bool reverse = false}) {
    var values = PlaybackRate.values;
    var i = values.indexOf(this);
    if (reverse) {
      return i == values.length - 1 ? values.first : values[i + 1];
    }
    return i == 0 ? values.last : values[i - 1];
  }
}