// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// Youtube Errors
enum YoutubeError {
  /// Error Free
  none,

  /// The request contains an invalid parameter value.
  ///
  /// For example, this error occurs if you specify a video ID that does not have 11 characters,
  /// or if the video ID contains invalid characters, such as exclamation points or asterisks.
  invalidParam,

  /// The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.
  html5Error,

  /// The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private.
  videoNotFound,

  /// The owner of the requested video does not allow it to be played in embedded players.
  notEmbeddable,

  /// This error is the same as [YoutubeError.notEmbeddable] in disguise!
  notEmbeddable2,

  /// Unknown Error
  unknown,
}

extension IntYoutubeErrorExtensions on int {
  YoutubeError toYoutubeError() =>
      YoutubeErrorInfo.fromErrorCode(this).youtubeError;
}

extension YoutubeErrorExtensions on YoutubeError {
  int get errorCode => YoutubeErrorInfo.fromYoutubeError(this).errorCode;
  String get name => YoutubeErrorInfo.fromYoutubeError(this).name;
  String get message => YoutubeErrorInfo.fromYoutubeError(this).message;
  _YoutubeErrorInfo get info => YoutubeErrorInfo.fromYoutubeError(this);
}

class _YoutubeErrorInfo {
  final int errorCode;
  final YoutubeError youtubeError;
  final String name;
  final String message;

  const _YoutubeErrorInfo({
    required this.youtubeError,
    required this.errorCode,
    required this.name,
    required this.message
  });
}

// ignore: avoid_classes_with_only_static_members
class YoutubeErrorInfo {
  static const none = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.none,
    errorCode: 0,
    name: "None",
    message: "Do not try and report the error—that's impossible. Instead, "
        "only try to realize the truth... There is no error.",
  );

  static const invalidParam = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.invalidParam,
    errorCode: 2,
    name: "Invalid Parameter",
    message: "The request contains an invalid parameter value. For example, "
        "this error occurs if you specify a video ID that does not have 11 "
        "characters, or if the video ID contains invalid characters, such as "
        "exclamation points or asterisks.",
  );

  static const html5Error = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.html5Error,
    errorCode: 5,
    name: "HTML5 Error",
    message: "The requested content cannot be played in an HTML5 player or "
        "another error related to the HTML5 player has occurred.",
  );

  static const videoNotFound = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.videoNotFound,
    errorCode: 100,
    name: "Video Not Found",
    message: "The video requested was not found. This error occurs when a "
        "video has been removed (for any reason) or has been marked as "
        "private.",
  );

  static const notEmbeddable = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.notEmbeddable,
    errorCode: 101,
    name: "Embed Disabled",
    message: "The owner of the requested video does not allow it to be "
        "played in embedded players.",
  );

  static const notEmbeddable2 = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.notEmbeddable2,
    errorCode: 150,
    name: "Embed Disabled",
    message: "The owner of the requested video does not allow it to be "
        "played in embedded players.",
  );

  static const unknown = const _YoutubeErrorInfo(
    youtubeError: YoutubeError.unknown,
    errorCode: -1,
    name: "Unknown Error",
    message: "Unknown Error",
  );

  static const List<_YoutubeErrorInfo> values = const [none, invalidParam,
    html5Error, videoNotFound, notEmbeddable, notEmbeddable2, unknown];

  static _YoutubeErrorInfo fromErrorCode(int errorCode) =>
      values.firstWhere((it) => it.errorCode == errorCode);

  static _YoutubeErrorInfo fromYoutubeError(YoutubeError youtubeError) =>
      values.firstWhere((it) => it.youtubeError == youtubeError);

  static _YoutubeErrorInfo fromName(String name) =>
      values.firstWhere((it) => it.name == name);

  static _YoutubeErrorInfo fromMessage(String message) =>
      values.firstWhere((it) => it.message == message);
}