// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/// Current state of the player.
///
/// Find more about it [here](https://developers.google.com/youtube/iframe_api_reference#Playback_status).
enum PlayerState {
  /// Denotes State when player is not loaded with video.
  unknown,

  /// Denotes state when player loads first video.
  unStarted,

  /// Denotes state when player has ended playing a video.
  ended,

  /// Denotes state when player is playing video.
  playing,

  /// Denotes state when player is paused.
  paused,

  /// Denotes state when player is buffering bytes from the internet.
  buffering,

  /// Denotes state when player loads video and is ready to be played.
  cued,
}

///
extension PlayerStateIntExtensions on int {
  ///
  PlayerState toPlayerState() {
    switch (this) {
      case -1:
        return PlayerState.unStarted;
      case 0:
        return PlayerState.ended;
      case 1:
        return PlayerState.playing;
      case 2:
        return PlayerState.paused;
      case 3:
        return PlayerState.buffering;
      case 5:
        return PlayerState.cued;
      default:
        return PlayerState.unknown;
    }
  }
}

///
extension PlayerStateExtensions on PlayerState {
  /// Returns [true] if the video has not yet started
  bool isUnStarted() => this == PlayerState.unStarted;

  /// Returns [true] if the video has ended
  bool isEnded() => this == PlayerState.ended;

  /// Returns [true] if the video is playing
  bool isPlaying() => this == PlayerState.playing;

  /// Returns [true] if the video is paused
  bool isPaused() => this == PlayerState.paused;

  /// Returns [true] if the video is buffering
  bool isBuffering() => this == PlayerState.buffering;

  /// Returns [true] if the video is cued
  bool isCued() => this == PlayerState.cued;

  /// Returns [true] if the video state is not known
  bool isUnknown() => this == PlayerState.unknown;

  /// Returns [true] if the video has already started
  bool isNotUnStarted() => this != PlayerState.unStarted;

  /// Returns [true] if the video has not ended
  bool isNotEnded() => this != PlayerState.ended;

  /// Returns [true] if the video is not playing
  bool isNotPlaying() => this != PlayerState.playing;

  /// Returns [true] if the video is not paused
  bool isNotPaused() => this != PlayerState.paused;

  /// Returns [true] if the video is not buffering
  bool isNotBuffering() => this != PlayerState.buffering;

  /// Returns [true] if the video is not cued
  bool isNotCued() => this != PlayerState.cued;

  /// Returns [true] if the video state is known
  bool isNotUnknown() => this != PlayerState.unknown;
}
