// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;
import 'package:youtube_player_iframe/src/enums/youtube_error.dart';
import 'package:youtube_player_iframe/src/helpers/player_fragments.dart';
import 'package:youtube_player_iframe/src/player_value.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../controller.dart';
import '../enums/player_state.dart';
import '../meta_data.dart';

/// A youtube player widget which interacts with the underlying webview inorder to play YouTube videos.
///
/// Use [YoutubePlayerIFrame] instead.
class RawYoutubePlayer extends StatefulWidget {
  /// The [YoutubePlayerController].
  final YoutubePlayerController controller;

  /// Which gestures should be consumed by the youtube player.
  ///
  /// It is possible for other gesture recognizers to be competing with the player on pointer
  /// events, e.g if the player is inside a [ListView] the [ListView] will want to handle
  /// vertical drags. The player will claim gestures that are recognized by any of the
  /// recognizers on this list.
  ///
  /// By default vertical and horizontal gestures are absorbed by the player.
  /// Passing an empty set will ignore the defaults.
  final Set<Factory<OneSequenceGestureRecognizer>>? gestureRecognizers;

  /// Creates a [RawYoutubePlayer] widget.
  const RawYoutubePlayer({
    Key? key,
    required this.controller,
    this.gestureRecognizers,
  }) : super(key: key);

  @override
  _MobileYoutubePlayerState createState() => _MobileYoutubePlayerState();
}

class _MobileYoutubePlayerState extends State<RawYoutubePlayer>
    with WidgetsBindingObserver {
  late final YoutubePlayerController _controller;
  late final Completer<InAppWebViewController> _webController;
  PlayerState? _cachedPlayerState;
  bool _isPlayerReady = false;
  bool _onLoadStopCalled = false;

  @override
  void initState() {
    super.initState();
    _webController = Completer();
    _controller = widget.controller;
    WidgetsBinding.instance?.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        if (_cachedPlayerState?.isPlaying() ?? false) {
          _controller.play();
        }
        break;
      case AppLifecycleState.paused:
        _cachedPlayerState = _controller.value.playerState;
        _controller.pause();
        break;
      default:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InAppWebView(
      key: ValueKey(_controller.hashCode),
      initialData: InAppWebViewInitialData(
        data: player,
        baseUrl: _baseUrl,
        encoding: 'utf-8',
        mimeType: 'text/html',
      ),
      gestureRecognizers: _gestureRecognizers,
      initialOptions: InAppWebViewGroupOptions(
        crossPlatform: InAppWebViewOptions(
          userAgent: userAgent,
          mediaPlaybackRequiresUserGesture: false,
          transparentBackground: true,
          disableContextMenu: true,
          supportZoom: false,
          disableHorizontalScroll: false,
          disableVerticalScroll: false,
          useShouldOverrideUrlLoading: true,
        ),
        ios: IOSInAppWebViewOptions(
          allowsInlineMediaPlayback: true,
          allowsAirPlayForMediaPlayback: true,
          allowsPictureInPictureMediaPlayback: true,
        ),
        android: AndroidInAppWebViewOptions(
          useWideViewPort: false,
          useHybridComposition: _controller.params.useHybridComposition,
        ),
      ),
      shouldOverrideUrlLoading: _decideNavigationActionPolicy,
      onWebViewCreated: (webController) {
        if (!_webController.isCompleted) {
          _webController.complete(webController);
        }
        _controller.invokeJavascript = _callMethod;
        _addHandlers(webController);
      },
      onLoadStop: (_, __) {
        _onLoadStopCalled = true;
        if (_isPlayerReady) {
          _controller.handle(YoutubeStateHandler.Ready);
        }
      },
      onConsoleMessage: (_, message) => log(message.message),
      onEnterFullscreen: (_) => _controller.onEnterFullscreen?.call(),
      onExitFullscreen: (_) => _controller.onExitFullscreen?.call(),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  Uri get _baseUrl => Uri.parse(
    'https://www.youtube${_controller.params.privacyEnhanced?'-nocookie':''}.com'
  );

  Set<Factory<OneSequenceGestureRecognizer>> get _gestureRecognizers {
    return widget.gestureRecognizers ?? {
      Factory<VerticalDragGestureRecognizer>(
        () => VerticalDragGestureRecognizer(),
      ),
      Factory<HorizontalDragGestureRecognizer>(
        () => HorizontalDragGestureRecognizer(),
      ),
    };
  }

  Future<dynamic> _callMethod(String methodName) async {
    log('JAVASCRIPT: $methodName');
    final webController = await _webController.future;
    return webController.evaluateJavascript(source: methodName);
  }

  void _addHandlers(InAppWebViewController webController) {
    webController
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.Ready,
        callback: (_) {
          _isPlayerReady = true;
          if (_onLoadStopCalled) {
            _controller.handle(YoutubeStateHandler.Ready);
          }
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.StateChange,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.StateChange, args);
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.PlaybackQualityChange,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.PlaybackQualityChange, args);
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.PlaybackRateChange,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.PlaybackRateChange, args);
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.Errors,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.Errors, args);
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.VideoData,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.VideoData, args);
        },
      )
      ..addJavaScriptHandler(
        handlerName: YoutubeStateHandler.VideoTime,
        callback: (args) {
          _controller.handle(YoutubeStateHandler.VideoTime, args);
        },
      )
      ..addJavaScriptHandler(handlerName: 'Log', callback: (args) {
        print('PLAYER LOG: ${args.first}');
      });

      // TODO: maybe instead of stopping videotime, we just slow it down for buffering??

      // ..addJavaScriptHandler(
      //   handlerName: 'Buffered',
      //   callback: (args) {
      //     final num buffered = args.first;
      //     _value = _value.copyWith(
      //       buffered: buffered.toDouble(),
      //       isUpdatingBuffered: true,
      //     );
      //   },
      // );
  }

  Future<NavigationActionPolicy?> _decideNavigationActionPolicy(
    _,
    NavigationAction action,
  ) async {
    final uri = action.request.url;
    if (uri == null) return NavigationActionPolicy.CANCEL;

    final params = uri.queryParameters;
    final host = uri.host;

    String? featureName;
    if (host.contains('facebook') || uri.host.contains('twitter')) {
      featureName = 'social';
    } else if (params.containsKey('feature')) {
      featureName = params['feature'];
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      return NavigationActionPolicy.ALLOW;
    }

    switch (featureName) {
      case 'emb_title':
      case 'emb_rel_pause':
      case 'emb_rel_end':
        final videoId = params['v'];
        if (videoId != null) _controller.load(videoId);
        break;
      case 'emb_logo':
      case 'social':
      case 'wl_button':
        url_launcher.launch(uri.toString());
        break;
    }

    return NavigationActionPolicy.CANCEL;
  }

  String get player => '''
    <!DOCTYPE html>
    <body>
        ${youtubeIFrameTag(_controller)}
        <script>
            $initPlayerIFrame
            var player;
            var timerId;
            // var bufferTimerId;
            var isLoadingMetadata = false;
            function onYouTubeIframeAPIReady() {
                player = new YT.Player('player', {
                    events: {
                        onReady: function(it) { 
                          onPlayerReady(it.target); 
                        },
                        onStateChange: function(it) { 
                          sendPlayerStateChange(it.data); 
                        },
                        onPlaybackQualityChange: function(it) {
                          window.flutter_inappwebview.callHandler(
                              '${YoutubeStateHandler.PlaybackQualityChange}', 
                              it.data,
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ); 
                        },
                        onPlaybackRateChange: function(it) { 
                          window.flutter_inappwebview.callHandler(
                              '${YoutubeStateHandler.PlaybackRateChange}', 
                              it.data,
                              // todo: could there be an issue accessing before assignment??
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ); 
                        },
                        onError: function(error) { 
                          window.flutter_inappwebview.callHandler(
                              '${YoutubeStateHandler.Errors}', 
                              error.data,
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ); 
                        }
                    },
                });
            }
            
            function log(msg) {
              window.flutter_inappwebview.callHandler('Log', msg);
            }
            
            function onPlayerReady(player) {
                // todo: consider the "hasPlayed" boolean here?
                if (${_controller.params.autoLoad} || ${_controller.params.autoPlay}) {
                    log("autoLoading");
                    isLoadingMetadata = true;
                    player.playVideo();
                }
                window.flutter_inappwebview.callHandler('${YoutubeStateHandler.Ready}');
                // startSendCurrentBufferedInterval();
            }

            function sendPlayerStateChange(playerState) {
                if (isLoadingMetadata) {
                    if (playerState == 1) {
                        if (${!_controller.params.mute}) player.unMute();
                        isLoadingMetadata = false;
                        sendVideoData(player);
                        if (${!_controller.params.autoPlay}) {
                            player.pauseVideo();
                            return;
                        }
                    } else {
                        return;
                    }
                }
                stopSendCurrentTimeInterval();
                window.flutter_inappwebview.callHandler('${YoutubeStateHandler.StateChange}', 
                    playerState,
                    player.getCurrentTime(), 
                    player.getVideoLoadedFraction()
                );
                log("playerstatechange::: " + playerState.toString());
                if (playerState == 1) {
                    // todo, should this be started even before playing??
                    startSendCurrentTimeInterval(
                        ${_controller.params.videoTimeUpdateFrequency}
                    );
                    sendVideoData(player);
                } else {
                    startSlowSendCurrentTimeInterval(250);
                }
            }

            function sendVideoData(player) {
                var videoData = {
                    'duration': player.getDuration(),
                    'title': player.getVideoData().title,
                    'author': player.getVideoData().author,
                    'videoId': player.getVideoData().video_id
                };
                window.flutter_inappwebview.callHandler('${YoutubeStateHandler.VideoData}', videoData);
            }
            
            function stopSendCurrentTimeInterval() {
              clearInterval(timerId);
            }

            function startSendCurrentTimeInterval(rate) {
                log("starting timer");
                timerId = setInterval(function () {
                    window.flutter_inappwebview.callHandler('${YoutubeStateHandler.VideoTime}', 
                        player.getCurrentTime(), 
                        player.getVideoLoadedFraction()
                    );
                }, rate);
            }
            $youtubeIFrameFunctions
        </script>
    </body>
  ''';

  String get userAgent => _controller.params.desktopMode
      ? 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'
      : '';
}
