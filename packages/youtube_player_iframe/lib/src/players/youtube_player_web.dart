// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/src/enums/player_state.dart';
import 'package:youtube_player_iframe/src/enums/youtube_error.dart';
import 'package:youtube_player_iframe/src/helpers/player_fragments.dart';
import 'package:youtube_player_iframe/src/enums/playback_rate.dart';
import 'package:youtube_player_iframe/extensions.dart';

import '../controller.dart';
import '../meta_data.dart';
import 'platform_view_stub.dart' if (dart.library.html) 'dart:ui' as ui;

/// A youtube player widget which interacts with the underlying iframe inorder to play YouTube videos.
///
/// Use [YoutubePlayerIFrame] instead.
class RawYoutubePlayer extends StatefulWidget {
  /// The [YoutubePlayerController].
  final YoutubePlayerController controller;

  /// no-op
  final Set<Factory<OneSequenceGestureRecognizer>>? gestureRecognizers;

  /// Creates a [MobileYoutubePlayer] widget.
  const RawYoutubePlayer({
    Key? key,
    required this.controller,
    this.gestureRecognizers,
  }) : super(key: key);

  @override
  _WebYoutubePlayerState createState() => _WebYoutubePlayerState();
}

class _WebYoutubePlayerState extends State<RawYoutubePlayer> {
  late YoutubePlayerController _controller;
  late Completer<IFrameElement> _iFrame;

  void tryHandle(String handlerName, Map<String, dynamic> data) {
    var args = data[handlerName];
    if (args != null) {
      _controller.handle(handlerName, args);
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = widget.controller;
    _iFrame = Completer();
    final playerIFrame = IFrameElement()
      ..srcdoc = player
      ..style.border = 'none';
    ui.platformViewRegistry.registerViewFactory(
      'youtube-player-${_controller.hashCode}',
      (int id) {
        window.onMessage.listen(
          (event) {
            final Map<String, dynamic> data = jsonDecode(event.data);
            tryHandle(YoutubeStateHandler.Ready, data);
            tryHandle(YoutubeStateHandler.StateChange, data);
            tryHandle(YoutubeStateHandler.PlaybackQualityChange, data);
            tryHandle(YoutubeStateHandler.PlaybackRateChange, data);
            tryHandle(YoutubeStateHandler.Errors, data);
            tryHandle(YoutubeStateHandler.VideoData, data);
            tryHandle(YoutubeStateHandler.VideoTime, data);
          },
        );
        if (!_iFrame.isCompleted) {
          _iFrame.complete(playerIFrame);
        }
        _controller.invokeJavascript = _callMethod;
        return playerIFrame;
      },
    );
  }

  Future<void> _callMethod(String methodName) async {
    final iFrame = await _iFrame.future;
    iFrame.contentWindow?.postMessage(methodName, '*');
  }

  @override
  Widget build(BuildContext context) {
    return HtmlElementView(
      key: ObjectKey(_controller),
      viewType: 'youtube-player-${_controller.hashCode}',
    );
  }

  String get player => '''
    <!DOCTYPE html>
    <body>
        ${youtubeIFrameTag(_controller)}
        <script>
            $initPlayerIFrame
            var player;
            var timerId;
            // var bufferTimerId;
            var isLoadingMetadata = false;
            var hasPlayed = false;
            function onYouTubeIframeAPIReady() {
                player = new YT.Player('player', {
                    events: {
                        onReady: (it) => onPlayerReady(it.target),
                        onStateChange: (it) => sendPlayerStateChange(it.data),
                        onPlaybackQualityChange: (it) => sendMessage({ 
                          '${YoutubeStateHandler.PlaybackQualityChange}': [
                              it.data, 
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ] 
                        }),
                        onPlaybackRateChange: (it) => sendMessage({ 
                          '${YoutubeStateHandler.PlaybackRateChange}': [
                              it.data, 
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ] 
                        }),
                        onError: (it) => sendMessage({ 
                          '${YoutubeStateHandler.Errors}': [
                              it.data, 
                              player.getCurrentTime(), 
                              player.getVideoLoadedFraction()
                          ] 
                        }),
                    },
                });
            }
            
            window.addEventListener('message', (event) => {
               try { eval(event.data) } catch (e) {}
            }, false);
            
            function sendMessage(message) {
              window.parent.postMessage(JSON.stringify(message), '*');
            }
            
            function onPlayerReady(player) {
                // todo: consider the "hasPlayed" boolean here?
                if (${_controller.params.autoLoad && !_controller.params.autoPlay}) {
                    isLoadingMetadata = true;
                    player.playVideo();
                }
                sendMessage({ ${YoutubeStateHandler.Ready}: [true] });
                // startSendCurrentBufferedInterval();
            }

            function sendPlayerStateChange(playerState) {
                if (isLoadingMetadata) {
                    if (playerState == 1) {
                        if (${!_controller.params.mute}) player.unMute();
                        isLoadingMetadata = false;
                        sendVideoData(player);
                        if (${!_controller.params.autoPlay}) {
                            player.pauseVideo();
                            return;
                        }
                    } else {
                        return;
                    }
                }
                clearTimeout(timerId);
                sendMessage({ 
                    '${YoutubeStateHandler.StateChange}': [
                        playerState,
                        player.getCurrentTime(), 
                        player.getVideoLoadedFraction()
                    ]
                });
                if (playerState == 1) {
                    // if (!hasPlayed) {
                    //   hasPlayed = true;
                    if (${!_controller.params.mute}) player.unMute();
                    startSendCurrentTimeInterval(
                        ${_controller.params.videoTimeUpdateFrequency}
                    );
                    // }
                    // startSendCurrentTimeInterval();
                    sendVideoData(player);
                } else {
                    startSendCurrentTimeInterval(250);
                }
            }

            function sendVideoData(player) {
                var videoData = {
                    'duration': player.getDuration(),
                    'title': player.getVideoData().title,
                    'author': player.getVideoData().author,
                    'videoId': player.getVideoData().video_id
                };
                sendMessage({ 'VideoData': [videoData] });
            }

            function startSendCurrentTimeInterval(rate) {
                timerId = setInterval(function () {
                    sendMessage({ 
                        '${YoutubeStateHandler.VideoTime}': [
                            player.getCurrentTime(), 
                            player.getVideoLoadedFraction()
                        ]
                    });
                }, rate);
            }
            
            // function startSendCurrentBufferedInterval() {
            //     bufferTimerId = setInterval(function () {
            //         sendMessage({ 'Buffered': player.getVideoLoadedFraction() });
            //     }, 200);
            // }
            
            $youtubeIFrameFunctions
        </script>
    </body>
  ''';
}
