/// A base callback class that allows a user to pause or cancel it. It is
/// essentially a more powerful [Function].
class Callback {

  bool _isCanceled = false;
  bool _isPaused = false;

  /// Returns whether or not the callback is currently paused.
  bool get isPaused  => _isPaused;

  /// Returns whether or not the callback has been canceled.
  bool get isCanceled  => _isCanceled;

  /// The action that represents the callback.
  final Function _action;

  ///
  late final Function action;

  /// Called when the action throws an [Error].
  final Function? onError;

  /// Called when the action throws an [Exception].
  final Function? onException;

  /// Called when the action throws an [Error] or an [Exception].
  final Function? onExceptionOrError;

  /// Create a callback that uses as action.
  Callback(this._action, {this.onError, this.onException, this.onExceptionOrError});

  /// Cancel the callback permanently.
  void cancel() {
    _isCanceled = true;
  }

  /// Temporarily pause the callback.
  void pause() {
    _isPaused = true;
  }

  /// Resume a paused callback.
  void resume() {
    _isPaused = false;
  }

}