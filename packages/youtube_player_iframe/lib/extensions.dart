
///
extension AsExtension on Object? {
  /// Returns the receiver cast to the type parameter, or null if it can't be
  /// cast. Cannot use on [dynamic]-typed variables.
  X? as<X>() => this is X ? this as X : null;
}
