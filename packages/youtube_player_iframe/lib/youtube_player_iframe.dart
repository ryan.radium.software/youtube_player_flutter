// Copyright 2020 Sarbagya Dhaubanjar. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:youtube_player_iframe/src/enums/playback_rate.dart';
import 'package:youtube_player_iframe/src/libs/audio_video_progress_bar.dart';
import 'package:youtube_player_iframe/src/player_value.dart';

import 'src/controller.dart';
import 'src/helpers/youtube_value_provider.dart';

import 'src/players/youtube_player_mobile.dart'
    if (dart.library.html) 'src/players/youtube_player_web.dart';

export 'src/controller.dart';
export 'src/enums/playback_rate.dart';
export 'src/enums/player_state.dart';
export 'src/enums/playlist_type.dart';
export 'src/enums/thumbnail_quality.dart';
export 'src/enums/youtube_error.dart';
export 'src/helpers/youtube_value_builder.dart';
export 'src/helpers/youtube_value_provider.dart';
export 'src/meta_data.dart';
export 'src/player_params.dart';

/// A widget to play or stream Youtube Videos.
class YoutubePlayerIFrame extends StatelessWidget {
  /// The [controller] for this player.
  final YoutubePlayerController? controller;

  /// Aspect ratio for the player.
  final double aspectRatio;

  /// Which gestures should be consumed by the youtube player.
  ///
  /// It is possible for other gesture recognizers to be competing with the player on pointer
  /// events, e.g if the player is inside a [ListView] the [ListView] will want to handle
  /// vertical drags. The player will claim gestures that are recognized by any of the
  /// recognizers on this list.
  ///
  /// By default vertical and horizontal gestures are absorbed by the player.
  /// Passing an empty set will ignore the defaults.
  ///
  /// This is ignored on web.
  final Set<Factory<OneSequenceGestureRecognizer>>? gestureRecognizers;

  /// A widget to play or stream Youtube Videos.
  const YoutubePlayerIFrame({
    Key? key,
    this.controller,
    this.aspectRatio = 16 / 9,
    this.gestureRecognizers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: aspectRatio,
      child: RawYoutubePlayer(
        controller: controller ?? context.ytController,
        gestureRecognizers: gestureRecognizers,
      ),
    );
  }
}

///
class YoutubePlayer extends StatefulWidget {
  ///
  final YoutubePlayerController controller;

  /// Aspect ratio for the player.
  final double aspectRatio;

  ///
  final SeekBarParams seekBarParams;

  /// Allows you to inject a CustomPainter to paint over the video
  final CustomPainter? videoPainter;

  /// Whether or not to enable clip mode. Clip mode means that the [startAt] and
  /// [endAt] fields are treated as the full length of the video.
  final bool clipMode;

  ///
  const YoutubePlayer({
    Key? key,
    required this.controller,
    this.aspectRatio = 16 / 9,
    this.seekBarParams = const SeekBarParams(),
    this.videoPainter,
    this.clipMode = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _YoutubePlayerState();
}

class _YoutubePlayerState extends State<YoutubePlayer> with SingleTickerProviderStateMixin {

  @override
  void setState(fn) {
    if(mounted) {
      super.setState(fn);
    }
  }

  late AnimationController _animationController;
  bool playing = false;

  static const Duration _bigDuration = const Duration(days: 100);

  Duration _currTime = Duration.zero;
  Duration _duration = _bigDuration;
  Duration _buffered = Duration.zero;
  bool isSeeking = false;
  PlayerCallback? afterSeekCallback;
  PlaybackRate _playbackRate = PlaybackRate.normal;

  Duration get duration {
    if (widget.clipMode) {
      return (widget.controller.params.endAt ?? _duration) - widget.controller.params.startAt;
    } else {
      return _duration;
    }
  }

  Duration get currTime {
    if (widget.clipMode) {
      return _currTime - widget.controller.params.startAt;
    } else {
      return _currTime;
    }
  }

  Duration get buffered {
    if (widget.clipMode) {
      return _buffered - widget.controller.params.startAt;
    } else {
      return _buffered;
    }
  }

  @override
  void initState() {
    super.initState();
    widget.controller.params = widget.controller.params.copyWith(showControls: false, showFullscreenButton: false);
    widget.controller.hideTopMenu();
    widget.controller.hidePauseOverlay();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 250),
      reverseDuration: const Duration(milliseconds: 250),
    );
    _animationController.value = 1;
    _currTime = widget.controller.params.startAt;
    widget.controller
      ..onMetadataLoaded((event) {
        setState(() {
          _duration = event.videoLength;
        });
      })
      ..onPlayerTimeUpdated((event) {
        if (afterSeekCallback?.isCanceled ?? true) {
          setState(() {
            _currTime = event.position;
          });
        }
      })
      ..onBufferedAmountChanged((event) {
        setState(() {
          _buffered = event.bufferedDuration;
        });
      })
      ..onVideoPause((event) {
        _onVideoPlaying(false);
      })
      ..onVideoPlay((event) {
        _onVideoPlaying(true);
      })
      ..onPlaybackRateChanged((event) {
        setState(() {
          _playbackRate = event.playbackRate;
        });
      })..onVideoEnd((event) {
        _onVideoPlaying(false);
      })..onVideoBuffering((event) {
        _onVideoPlaying(false);
      });
  }

  void _onVideoPlaying(bool isPlaying) {
    if (playing && !isPlaying) {
      _animationController.forward();
    } else if (!playing && isPlaying) {
      _animationController.reverse();
    }
    playing = isPlaying;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // todo: consider wrapping with [ClipRect]?
        CustomPaint(
          foregroundPainter: widget.videoPainter,
          child: YoutubePlayerIFrame(
            gestureRecognizers: {},
            aspectRatio: widget.aspectRatio,
            controller: widget.controller,
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: widget.seekBarParams.backgroundColor,
          ),
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: ProgressBar(
                  progress: currTime,
                  buffered: buffered,
                  // todo: clip mode
                  total: duration,
                  progressBarColor: widget.seekBarParams.progressColor,
                  baseBarColor: widget.seekBarParams.baseColor,
                  bufferedBarColor: widget.seekBarParams.bufferedColor,
                  thumbColor: widget.seekBarParams.thumbColor,
                  barHeight: widget.seekBarParams.barHeight,
                  thumbRadius: widget.seekBarParams.thumbRadius,
                  thumbGlowColor: widget.seekBarParams.thumbGlowColor,
                  thumbGlowRadius: widget.seekBarParams.thumbGlowRadius,
                  timeLabelLocation: TimeLabelLocation.none,
                  barCapShape: BarCapShape.square,

                  onSeek: (duration) {
                    setState(() {
                      if (widget.clipMode) {
                        _currTime = duration + widget.controller.params.startAt;
                      } else {
                        _currTime = duration;
                      }
                      widget.controller.seekTo(_currTime);
                      isSeeking = false;
                    });
                  },
                  onDragStart: (details) {
                    setState(() {
                      isSeeking = true;
                      afterSeekCallback = widget.controller.onVideoPlay((event) {
                        if (!isSeeking) {
                          afterSeekCallback?.cancel();
                        }
                      });
                    });
                  },
                  onDragUpdate: (details) {
                    setState(() {
                      if (widget.clipMode) {
                        _currTime = details.timeStamp + widget.controller.params.startAt;
                      } else {
                        _currTime = details.timeStamp;
                      }
                    });
                  },
                  onTap: (details) {
                    setState(() {
                      isSeeking = true;
                      afterSeekCallback = widget.controller.onVideoPlay((_) {
                        if (!isSeeking) {
                          afterSeekCallback?.cancel();
                        }
                      });
                      if (widget.clipMode) {
                        _currTime = details.timeStamp + widget.controller.params.startAt;
                      } else {
                        _currTime = details.timeStamp;
                      }
                      widget.controller.seekTo(_currTime);
                    });
                  },
                  thumbCanPaintOutsideBar: false,
                ),
              ),
              Row(
                children: [
                  IconButton(
                    constraints: const BoxConstraints(
                        maxWidth: 40,
                        minWidth: 40,
                        maxHeight: 40,
                        minHeight: 40
                    ),
                    iconSize: 32,
                    padding: const EdgeInsets.all(4.0),
                    icon: AnimatedIcon(
                      icon: AnimatedIcons.pause_play,
                      progress: _animationController,
                      semanticLabel: 'Play',
                      color: widget.seekBarParams.buttonColor.withOpacity(1.0),
                    ),
                    onPressed: () {
                      print('${widget.controller.value}');
                      if (!playing) {
                        widget.controller.play();
                      } else {
                        widget.controller.pause();
                      }
                    },
                  ),
                  Text(
                    '${currTime.toTimeString()} / ${duration.toTimeString()}',
                    style: widget.seekBarParams.textStyle.copyWith(
                      color:
                          duration == _bigDuration ? Colors.transparent : null,
                    ),
                  ),
                  const Expanded(child: const SizedBox()),
                  TextButton(
                      onPressed: () {
                        setState(() {
                          widget.controller.setPlaybackRate(_playbackRate.next());
                        });
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            Colors.transparent.toMaterialStateColor(),
                        overlayColor: Colors.transparent.toMaterialStateColor(),
                        shadowColor: Colors.transparent.toMaterialStateColor(),
                        alignment: Alignment.centerRight,
                        textStyle: TextStyle(
                          color: widget.seekBarParams.buttonColor,
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                        ).toMaterialStateTextStyle(),
                      ),
                      child: Text(
                        "${_playbackRate.toDouble()} x",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          color: widget.seekBarParams.buttonColor,
                          fontSize: 12,
                        ),
                      )),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}

///
extension DurationExtensions on Duration {
  ///
  String toTimeString() {
    final minutes =
        inMinutes.remainder(Duration.minutesPerHour).toString().padLeft(2, '0');
    final seconds = inSeconds
        .remainder(Duration.secondsPerMinute)
        .toString()
        .padLeft(2, '0');
    final hours = inHours > 0 ? '$inHours:' : '';
    return "$hours$minutes:$seconds";
  }
}

///
extension DoubleExtensions on double {
  ///
  bool isInt() => this == toInt();
}

///
extension ColorExtensions on Color {
  ///
  MaterialStateColor toMaterialStateColor(
      [Color Function(Set<MaterialState> states)? resolveWith]) {
    return MaterialStateColor.resolveWith(resolveWith ?? (_) => this);
  }
}

///
extension TextStyleExtensions on TextStyle {
  ///
  MaterialStateProperty<TextStyle> toMaterialStateTextStyle(
          [TextStyle Function(Set<MaterialState> states)? resolveWith]) =>
      MaterialStateProperty.resolveWith(resolveWith ?? (_) => this);
}

///
class SeekBarParams {
  ///
  final Color progressColor;

  ///
  final Color backgroundColor;

  ///
  final Color thumbColor;

  ///
  final Color baseColor;

  ///
  final Color bufferedColor;

  ///
  final double barHeight;

  ///
  final double thumbRadius;

  ///
  final TextStyle textStyle;

  ///
  final Color buttonColor;

  ///
  final double thumbGlowRadius;

  ///
  final Color? _thumbGlowColor;

  Color get thumbGlowColor => _thumbGlowColor ?? thumbColor.withOpacity(0.8);

  ///
  const SeekBarParams({
    this.progressColor = Colors.red,
    this.backgroundColor = Colors.black,
    this.thumbColor = Colors.white,
    this.baseColor = const Color(1040187391),
    this.bufferedColor = const Color(1040187391),
    this.barHeight = 10.0,
    this.thumbRadius = 10.0,
    Color? thumbGlowColor,
    this.thumbGlowRadius = 10.0,
    this.textStyle = const TextStyle(fontSize: 12, color: Colors.grey),
    this.buttonColor = Colors.grey,
  }) : _thumbGlowColor = thumbGlowColor;
}
